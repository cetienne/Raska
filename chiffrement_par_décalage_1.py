"""Alors que vous avez enfin trouvé le livre que vous cherchiez, il s’avère qu’il était complètement chiffré. Vous avez déjà réussi à déchiffrer la première page du livre, et celle-ci vous a indiqué le système de chiffrement utilisé dans le reste du livre.

La clé de chiffrement est ici un simple nombre, qu’on appelle D, pour « décalage ». On remplace alors chaque lettre de l’alphabet par la lettre située D places plus loin dans l’alphabet, considéré de manière circulaire.

Ainsi, si le décalage est de 2, alors

    A devient C
    B devient D
    ...
    X devient Z
    Y devient A
    Z devient B

La clé utilisée pour chiffrer le texte change à chaque page et pour la page numéro X elle vaut

    3 * X si X est pair
    - 5 * X si X est impair

À vous de déchiffrer tout le livre !
LIMITES DE TEMPS ET DE MEMOIRE (Langage : Python)

Temps : 1s sur une machine à 1Ghz.
Mémoire : 8000 Ko.
CONTRAINTES

Chaque ligne contient au plus 1000 caractères.
ENTRÉE

La première ligne contient un entier nbPages, le nombre total de pages du livre.

Les nbPages - 1 lignes suivantes contiennent chacune le texte des pages numéro 2, 3, 4,..., nbPages.

Le texte peut contenir des lettres, chiffres ou caractères de ponctuation, mais pas d’accents.
SORTIE

Vous devez afficher le texte déchiffré pour chacune des pages.

Chaque lettre codée doit être remplacé par la lettre décodée. Les autres caractères (ponctuation, '_', espaces, chiffres), sont laissés tels quels.

Vous devez respecter la casse : si une lettre était en majuscule (ou minuscule), elle doit le rester !
EXEMPLE

entrée :

4
Ikio kyz rg ykiutjk vgmk ja robxk
Npeep alrp pde wl alrp yfxpcz 3
Qf hauou pazo xm cgmfduqyq bmsq !

sortie :

Ceci est la seconde page du livre
Cette page est la page numero 3
Et voici donc la quatrieme page !"""

decrypteur = int(input())
alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
for page in range(2,decrypteur):
   texte = input()
   caractere = 0
   for loop in range(len(texte)):
      caractereLu = texte[caractere]
      if caractereLu.isalpha():
         if caractereLu.isupper():
            if page % 2 == 0:
               decalage = page * 3
               numero = ord(caractereLu.lower()) - ord('a')
               caractereLu = alphabet[numero - decalage].upper()               
            else:
               decalage = page * -5
               print(decalage)
               numero = ord(caractereLu.lower()) - ord('a')
               caractereLu = alphabet[numero - decalage].upper()            
         else:
            if page % 2 == 0:
               decalage = page * 3
               numero = ord(caractereLu) - ord('a')
               caractereLu = alphabet[numero - decalage]
            else:
               decalage = page * -5
               print(decalage)
               numero = ord(caractereLu) - ord('a')
               caractereLu = alphabet[numero - decalage]
      print(caractereLu, end = '')
      caractere = caractere + 1
   print()